import {getStations, createStation, getStationImage} from '../services/stations-requests.js';


async function renderStations() {

    // await possible du fait que "getStations" soit asynchrone !
    // Pas de problème puisque "renderStations" est aussi asynchrone et donc non bloquante
    let stations = await getStations();
    // let stations = [{ name: "Le haut", altitude: 4500 }, { name: "Le bas", altitude: 1500 }];
    
    stations.forEach(async station => {

        let container = document.querySelector("#cards-container");

        // création d'une div qui contiendra les informations
        // d'une station
        let stationCard = document.createElement('div');

        // on ajoute une class sur la nouvelle div
        // le style a son importance
        stationCard.classList.add('station-card');

        // il est possible de modifier directement le html entre les balises d'un élément
        // ceci grâce à "innerHTML"
        stationCard.innerHTML = `<h2>${station.name}</h2><p>Altitude : ${station.altitude}</p>`;

        let imageElement = document.createElement('img');
        // récupération des images

        let blobImage = await getStationImage(station.id);
        let objectURL = URL.createObjectURL(blobImage);
        imageElement.src = objectURL;

        stationCard.prepend(imageElement);
        // autre façon d'insérer du HTML dans une page
        // ici on ajoute le tout au container "beforeend"
        container.appendChild(stationCard);
    });
}


/*


You're setting the Content-Type to be multipart/form-data, but then using JSON.stringify on the body data, which returns application/json. You have a content type mismatch.

You will need to encode your data as multipart/form-data instead of json. Usually multipart/form-data is used when uploading files, and is a bit more complicated than application/x-www-form-urlencoded (which is the default for HTML forms).

The specification for multipart/form-data can be found in RFC 1867.

For a guide on how to submit that kind of data via javascript, see here.

The basic idea is to use the FormData object (not supported in IE < 10):

async function sendData(url, data) {
  const formData  = new FormData();

  for(const name in data) {
    formData.append(name, data[name]);
  }

  const response = await fetch(url, {
    method: 'POST',
    body: formData
  });

  // ...
}

Per this article make sure not to set the Content-Type header. The browser will set it for you, including the boundary parameter.
*/


async function addStation(event) {
    // par défaut le bouton essaie de rédir
    event.preventDefault();

    //récupération des information du formulaire
    let form = document.querySelector("#create-form");
    let formData = new FormData(form);
    console.log(formData);
    let jsonData = Object.fromEntries(formData.entries());
    
    // transofmration de la chaîne de caractères "altitude" en entier et màj du JSON
    jsonData.altitude = parseInt(jsonData.altitude);
    console.log("--- Requête de création d'une nouvelle station");
    console.log(jsonData);

    // récupération du toast pour affichage d'un message à l'utilisateur
    let toastElement = document.querySelector("#toast");

    try {
        // et maintenant on requête l'API ! Joie !
        let newStation = await createStation(formData);
        // Si la méthode retourne bien une station, c'est qu'elle a été créée
        toastElement.textContent = "Station créée avec succès";
        console.log("--- Nouvelle station créée");
        console.log(newStation);
    } catch (error) {
        toastElement.textContent = error;
    }

    // on affiche le toast
    toastElement.className = "show";
    // on cache le toast après 3 secondes
    setTimeout(function(){ toastElement.className = toastElement.className.replace("show", ""); }, 3000);
}

// Fonction se déclenchant quand le DOM a été chargé
// permet de lier le clique sur le bouton avec la fonction 
// de chargement des stations
window.addEventListener('DOMContentLoaded', event => {
    let displayButton = document.getElementById("display-button");
    //document.querySelector('#display-button');
    displayButton.addEventListener('click', renderStations);

    let addButton = document.querySelector('#add-button');
    addButton.addEventListener('click', addStation);
});